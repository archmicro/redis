# Redis
The repository contains manifests for the pod and service in the
Kubernetes cluster, CI/CD pipeline script. A standard redis:alpine
image is used. <br>
https://hub.docker.com/_/redis